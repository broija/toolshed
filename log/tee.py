#!/usr/local/bin/python3.7
# coding: utf-8

import sys

class Tee:
  """Substitute for Unix tee
      from : https://stackoverflow.com/questions/616645/how-do-i-duplicate-sys-stdout-to-a-log-file-in-python"""
# -----
  def __init__(self, log_path=None, mode='a'):
    """:param: log_path : if None, this object just prints output to stdout."""
    self.log_path = log_path
    self.mode = mode
# -----
  def __enter__(self):
    self.file = None
    self.stdout = None

    if self.log_path:
      self.stdout = sys.stdout

      # writing to stdout will now write to the Tee object
      sys.stdout = self

      self.file = open(self.log_path, self.mode)

    return self
# -----
  def __exit__(self, *args, **kwargs):
    if self.stdout:
      # giving stdout its former value
      sys.stdout = self.stdout

    if self.file:
      self.flush()
      self.file.close()
# -----
  def write(self, data):
    if self.file:
      self.file.write(data)
    self.stdout.write(data)
# -----
  def flush(self):
    if self.file:
      self.file.flush()