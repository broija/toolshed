# coding: utf-8

import pathlib
import platform

from .platform import *

_first_letters = platform.system().upper()[0:3]

class PathMapping:
    def __init__(self, linux=None, linux_vm=None, windows=None, cygwin=None, wsl=None):
        self.path = None
        if is_wsl():
            self.path = wsl
        elif is_linux_vm():
            self.path = linux_vm
        elif is_linux():
            self.path = linux
        elif is_windows():
            self.path = windows
        elif is_cygwin():
            self.path = cygwin

    def to_path(self):
        return pathlib.Path(self.path) if self.path else None

    def __str__(self):
        return self.path
