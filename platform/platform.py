#!/usr/bin/python3
# coding: utf-8

import os
import pathlib
import platform
import re
import subprocess

_system = platform.system().upper()
_mingw_drive_regex = None

# ----- -----

def is_cygwin():
  return _system.startswith('CYGWIN')

def is_windows():
  return _system.startswith('WIN')

def is_mingw():
  return _system.startswith('MINGW')

def is_linux():
  return _system.startswith('LINUX')

def is_wsl():
  return is_linux() and 'WSL' in os.uname().release

def is_linux_vm():
  return is_linux() and not virt_what()

# -----

def virt_what():
    result = False
    if subprocess.run(['which', 'virt-what'], stdout=subprocess.PIPE).stdout.decode():
        cache_path = pathlib.Path('{}/.virt-what'.format(os.environ['HOME']))

    if cache_path.is_file():
        result = cache_path.read_text()
    else:
        result = subprocess.run(['sudo', 'virt-what'], stdout=subprocess.PIPE).stdout.decode()
        with cache_path.open('w') as cache_file:
            cache_file.write(result)

    return result

def host_os_is_windows():
  return is_windows() or is_cygwin() or is_mingw() or is_wsl()

# ----- -----

def _mingw_path_check(path):
  if not isinstance(path, str):
    raise Exception('Unhandled path type.')

  return True

# -----

def _mingw_path_to_cygwin_path(path):
  """Converts MinGW path to Cygwin path."""

  newpath = pathlib.Path(path)

  newpath = pathlib.Path('/cygdrive') / (newpath.relative_to('/') if newpath.is_absolute() else newpath)

  if isinstance(path, str):
    return str(newpath)

  return newpath

# -----

def mingw_path_to_windows_path(path):
  """Converts MinGW path to Cygwin path."""

  path = str(path)

  return pathlib.PureWindowsPath(path[1] + ':' + path[2:])

# -----

def _cygwin_path_to_windows_path(path):
  newpath = mingw_path_to_windows_path(pathlib.Path('/') / pathlib.Path(path).relative_to('/cygdrive'))

  if isinstance(path, str):
    return str(newpath)

  return newpath

# -----

def _windows_path_to_cygwin_path(path):
  drive = path[0]

  return path.replace('\\','/').replace(drive + ':', '/cygdrive/' + drive)

# -----

def _mingw_path_to_wsl_path(path):
  """Converts MinGW path to WSL path."""

  result = pathlib.Path(path).absolute()
  strpath = str(result)

  if _mingw_drive_regex.match(strpath):
    result = pathlib.Path('/mnt' + strpath)

  return result if isinstance(path, pathlib.Path) else str(result)

# -----

def _wsl_path_to_windows_path(path):
    """Converts WSL path to Windows path."""

    result = None

    newpath = pathlib.Path(path).resolve()
    print(newpath.parts)
    if len(newpath.parts) > 3 and newpath.parts[1] == 'mnt' and len(newpath.parts[2]) == 1:
      relative = newpath.relative_to('/mnt')
      print(relative)
      result = mingw_path_to_windows_path('/' + str(relative))

    return result

_nope = lambda p: p

# -----

if is_cygwin():
  mingw_path_to_platform_path = _mingw_path_to_cygwin_path
  platform_path_to_windows_path = _cygwin_path_to_windows_path
  windows_path_to_platform_path = _windows_path_to_cygwin_path

elif is_windows():
  mingw_path_to_platform_path = mingw_path_to_windows_path
  platform_path_to_windows_path = _nope

elif is_wsl():
    _mingw_drive_regex = re.compile(r"[/][a-z]{1}($|[/][a-z/]*)")
    mingw_path_to_platform_path = _mingw_path_to_wsl_path
    platform_path_to_windows_path = _wsl_path_to_windows_path

else:
    mingw_path_to_platform_path = _mingw_path_to_wsl_path
    platform_path_to_windows_path = mingw_path_to_windows_path
