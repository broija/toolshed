#!/usr/bin/python3
# coding: utf-8

from .platform import *

get_platform_encoding = lambda: 'utf-8'

if host_os_is_windows():
  from .windows import *
else:
  def get_console_encoding():
    # bad: for windows files sync
    return 'cp437'
#    return get_platform_encoding()
