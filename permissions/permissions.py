#!/usr/bin/python3
# coding: utf-8

import pathlib
import shutil

def chown(path, user=None, group=None):
  if (not isinstance(path, pathlib.Path)):
    path = pathlib.Path(path)

  path = path.resolve()

  shutil.chown(str(path), user, group)

  for item in path.glob('**/*'):
    shutil.chown(str(item), user, group)
