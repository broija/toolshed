# coding: utf-8

import toolshed.platform

if toolshed.platform.is_linux():
  from toolshed.share.sshfs import *
elif toolshed.platform.host_os_is_windows():
  from toolshed.share.windows import *
