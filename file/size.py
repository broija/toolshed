#!/usr/bin/python3
# coding: utf-8

import os
import pathlib
import re

def get_size(start_path):
    start_path = pathlib.Path(start_path)
    for f in start_path.glob('**/*'):
      if f.is_file():
        print(f)
    # return sum(f.stat().st_size for f in start_path.glob('**/*') if f.is_file())

def _size(path):
  result = 0
  follow_symlinks=True

  for item in os.scandir(path):
    if item.is_dir(follow_symlinks=follow_symlinks):
      result += _size(item.path)
    else:
      result += item.stat(follow_symlinks=follow_symlinks).st_size

  return result

# -----

def size(path):
  if not os.path.exists(path):
    raise Exception('Path not found: {}'.format(path))

  if os.path.isfile(path):
    return os.stat(path, follow_symlinks=False).st_size

  return _size(path)

# -----

KB_SIZE = 1024
MB_SIZE = KB_SIZE * 1024
GB_SIZE = MB_SIZE * 1024
TB_SIZE = GB_SIZE * 1024

# -----

def float_to_str(size):
  suffix = ''

  if size >= TB_SIZE:
    size /= TB_SIZE
    suffix = 'T'

  elif size >= GB_SIZE:
    size /= GB_SIZE
    suffix = 'G'

  elif size >= MB_SIZE:
    size /= MB_SIZE
    suffix = 'M'

  elif size >= KB_SIZE:
    size /= KB_SIZE
    suffix = 'K'

  return str(round(size, 4)) + suffix

# -----

def str_to_float(size):
  match_res = str_to_float.regex.match(size.replace(',', '.'))

  if not match_res:
    raise Exception('Invalid size: {}'.format(size))

  suffix = size[-1]
  size = float(match_res.group(1))

  if suffix == 'T':
    size *= TB_SIZE
  elif suffix == 'G':
    size *= GB_SIZE
  elif suffix == 'M':
    size *= MB_SIZE
  elif suffix == 'K':
    size *= KB_SIZE

  return round(size, 4)

str_to_float.regex = re.compile('^([0-9\.]+)([TGMK]*)$')
