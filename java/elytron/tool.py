#!/usr/bin/python3
# coding: utf-8

import pathlib
import random
import string

from toolshed.docker.compose import Compose

class Tool:
  """WildFly elytron-tool CLI helper for docker"""

  class CLIError(Exception):
    def __init__(self, message, returncode=None):
      super().__init__()

      self.returncode = returncode

  def __init__(self, service, directory, debug=True):
    """:param directory: parent directory path."""

    if type(directory) is str:
      directory = pathlib.Path(directory)

    self.service = service
    self.path = directory / 'elytron-tool.sh'
    self.compose = Compose()

    self.debug = debug

  def cs_create(self, path, password=None):
    """Create credential store
    :param path: path to credential store.
    :param password: credential store password. Can be masked."""

    self._cs_exec(path, ['-c'], password=password)

  def cs_add_secret(self, path, alias, cs_password=None):
    """Add a secret to credential store.
    :param path: path to credential store.
    :param alias: secret name.
    :param cs_password: credential store password. Can be masked."""

    self._cs_exec(path, ['-a', alias], password=cs_password)

  def cs_remove_secret(self, path, alias, cs_password=None):
    """Remove a secret from credential store.
    :param path: path to credential store.
    :param alias: secret name.
    :param cs_password: credential store password. Can be masked."""

    self._cs_exec(path, ['-r', alias], password=cs_password)

  def cs_list_secret_aliases(self, path, cs_password=None):
    """List secret names in credential store.
    :param path: path to credential store.
    :param cs_password: credential store password. Can be masked."""

    return self._cs_exec(path, ['-v'], password=cs_password)

  def cs_secret_exists(self, path, alias, cs_password=None):
    result = None

    try:
      self._cs_exec(path, ['-e', alias], password=cs_password)

      result = True
    except self.__class__.CLIError as e:
      result = e.returncode == 0

    return result

  def mask(self, iteration, salt=None):
    """:param salt: 8 char salt. If left unset, a random salt will be generated."""

    if not salt:
      salt = ''.join(random.SystemRandom().choice(
        string.ascii_letters + string.digits + '!#$%&()*+,-./:<=>?@[]^_{|}~') for _ in range(8)
      )

    result = self._exec('mask', ['-i', str(iteration), '-s', salt], return_output=True)

    return self._get_lines(result)[-1]

  def _cs_exec(self, path, args, password=None, return_output=False):
    pargs = ['-l', str(path)]

    if password:
      pargs += ['-p', password]

    return self._exec('credential-store', pargs + args, return_output=return_output)

  def _exec(self, command, args, return_output=False):
    pargs = [self.service, str(self.path), command]

    if self.debug:
      pargs.append('-d')

    result = self.compose.exec(pargs + args, return_output=return_output)
    last_returncode = self.compose.get_last_returncode()

    if last_returncode != 0:
      raise self.__class__.CLIError("Last return code = {}".format(last_returncode), last_returncode)

    return result

  def _get_lines(self, string):
    return string.strip().replace("\r\n", "\n").split("\n")

if __name__ == '__main__':
  import argparse

  argparser = argparse.ArgumentParser()
  argparser.add_argument('service')
  argparser.add_argument('path', type=pathlib.Path, help='Parent directory path.')

  subparsers = argparser.add_subparsers(title='commands')

  mask_parser = subparsers.add_parser('mask', help='mask commands')
  mask_parser.add_argument('--iteration', '-i', default=2048)
  mask_parser.add_argument('--salt', '-s')

  args = argparser.parse_args()
  print(args)
  tool = Tool(args.service, args.path)

  if 'iteration' in args:
    tool.mask(args.iteration, salt=args.salt)
