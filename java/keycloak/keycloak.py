# coding: utf-8

from .vault.resolution import RealmUnderscoreKey as vault_res_RealmUnderscoreKey
from .vault.resolution import KeyOnly as vault_res_KeyOnly

class SPI:
  def __init__(self, name):
    self.name = name
    self.providers = []
    self.default_provider = None

  def set_default_provider(self, name):
    self.default_provider = name

  def add_provider(self, provider):
    self.providers.append(provider)

# -----

class Provider:
  def __init__(self, name):
    self.name = name
    self.properties = {}
    self.secrets = []

  def set_property(self, key, value):
    self.properties[key] = value

  def add_secret(self, secret):
    self.secrets.append(secret)

# -----

class Secret:
  def __init__(self, name, provider, realm=None):
    """:param provider: vault provider."""

    self.resolution = vault_res_RealmUnderscoreKey if realm else vault_res_KeyOnly

    self.name = self.resolution.get_secret_name(name, realm)
    self.provider = provider
    self.realm = realm

# -----

import json

from ..plugin import Plugin as JavaPlugin

class Plugin(JavaPlugin):
  def __init__(self, path):
    super().__init__(path)

    self.has_config = False

    module_json_path = path / 'module.json'
    plugin_json_path = path / 'plugin.json'

    if module_json_path.is_file():
      self.is_module = True
      plugin_json_path = module_json_path

    print(plugin_json_path)

    if self.is_module or plugin_json_path.is_file():
      self.has_config = True

      doc = json.load(plugin_json_path.open(mode='r'))
      print('Configuration loaded:', doc)
      self.full_name = doc['name']

      self.spis = []
      spi_list = doc['spi']

      if not self.is_module and 'module' in doc and doc['module'] == True:
        self.is_module = True

      if self.is_module:
        self.install = True

      if not self.install and 'install' in doc and doc['install'] == True:
        self.install = True

      for spi_json in spi_list:
        spi = SPI(spi_json['name'])

        if 'defaultProvider' in spi_json:
          spi.set_default_provider(spi_json['defaultProvider'])

        provider_list = spi_json['providers']
        for provider_json in provider_list:
          provider = Provider(provider_json['name'])

          if 'properties' in provider_json:
            properties = provider_json['properties']

            for key, value in properties.items():
              provider.set_property(key, value)

          if 'secrets' in provider_json:
            secrets = provider_json['secrets']

            for secret_name, secret_json in secrets.items():
              vault_provider = secret_json['provider']
              realm = secret_json['realm'] if 'realm' in secret_json else None

              if 'prefix' in secret_json:
                secret_name = secret_json['prefix'] + secret_name

              provider.add_secret(Secret(secret_name, vault_provider, realm=realm))

          spi.add_provider(provider)

        self.spis.append(spi)

  def get_spis(self):
    return self.spis
