# coding: utf-8

import abc
# import enum

# class ResolutionType(enum.Enum):
#   REALM_UNDERSCORE_KEY = 1
#   KEY_ONLY = 2
#   REALM_FILESEPARATOR_KEY = 3
#   FACTORY_PROVIDED = 4

# class UnhandledResolutionTypeError(Exception): pass
class UndefinedRealmError(Exception): pass

class ResolutionMetaClass(type):
  def __init__(cls, cls_name, parents, attr):
    cls.NAME = cls._get_name()

class AbstractResolutionMetaClass(ResolutionMetaClass, abc.ABCMeta): pass

# -----

class Resolution(metaclass=AbstractResolutionMetaClass):
  @classmethod
  @abc.abstractmethod
  def _get_name(cls): pass

  @staticmethod
  def is_realm_related():
    return False

  @staticmethod
  @abc.abstractmethod
  def get_secret_name(alias, realm=None): pass

# -----

class RealmUnderscoreKey(Resolution):
  @classmethod
  def _get_name(cls):
    return 'REALM_UNDERSCORE_KEY'

  @staticmethod
  def is_realm_related():
    return True

  @staticmethod
  def get_secret_name(alias, realm):
    if not realm:
      raise UndefinedRealmError(alias)

    return realm.replace('_','__') + '_' + alias.replace('_','__')

# -----

class RealmFileSeparatorKey(Resolution):
  @classmethod
  def _get_name(cls):
    return 'REALM_FILESEPARATOR_KEY'

  @staticmethod
  def is_realm_related():
    return True

  @staticmethod
  def get_secret_name(alias, realm):
    if not realm:
      raise UndefinedRealmError(alias)

    return realm + ':' + alias

# -----

class KeyOnly(Resolution):
  @classmethod
  def _get_name(cls):
    return 'KEY_ONLY'

  @staticmethod
  def get_secret_name(alias, realm=None):
    return alias
