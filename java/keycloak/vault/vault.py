#!/usr/bin/python3
# coding: utf-8

"""Keycloak vault tools"""

import abc

# -----

# class VaultType(enum.Enum):
#   ELYTRON_CS_KEYSTORE = 1
#   KUBERNETES = 2

class VaultMetaClass(type):
  def __init__(cls, cls_name, parents, attr):
    cls.NAME = cls._get_name()
    cls.LOCATION_ATTRIBUTE_NAME = cls._get_location_attribute_name()

    if cls_name != 'Vault':
      Vault._NAME_MAP[cls.NAME] = cls

class AbstractVaultMetaClass(VaultMetaClass, abc.ABCMeta): pass

# -----

class Vault(metaclass=AbstractVaultMetaClass):
  _NAME_MAP = {}

  @classmethod
  @abc.abstractmethod
  def _get_name(cls): pass

  @classmethod
  @abc.abstractmethod
  def _get_location_attribute_name(cls): pass

  @classmethod
  def name_to_class(cls, name):
    return cls._NAME_MAP[name]

# -----

class ElytronCSKeystore(Vault):
  @classmethod
  def _get_name(cls):
    return 'elytron-cs-keystore'

  @classmethod
  def _get_location_attribute_name(cls):
    return 'location'

# -----

class Kubernetes(Vault):
  @classmethod
  def _get_name(cls):
    return 'files-plaintext'

  @classmethod
  def _get_location_attribute_name(cls):
    return 'dir'

# -----
