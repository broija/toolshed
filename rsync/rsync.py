#!/usr/bin/python3
# coding: utf-8

import os
import pathlib
import re
import subprocess

import toolshed.platform

class RSyncError(Exception): pass
class RSyncNotLeftSpaceError(RSyncError): pass

class RSync:
    def __init__(self):
        self.files = None

    def sync(self, source, dest, port=None, source_user=None, user=None, dry_run=False,
            recursive=True, verbosity=1, checksum=False,
            compute_size=False, size_only=False,
            preserve_mtime=False, exclude_from=None, included_items=None,
            excluded_items=None, delete_during=None, filters=None,
            prune_empty_dirs=False, stats=False, changes=False, args=None,
            display=True, progress=False):

      pargs = ['rsync']

      if dry_run:
        pargs.append('--dry-run')

      if recursive:
        pargs.append('-r')

      if verbosity:
        pargs.append('-' + 'v' * verbosity)
      else:
        if display:
          display=False

      if port:
        pargs += ['-e', 'ssh -p {}'.format(port)]

      if checksum:
        pargs.append('--checksum')

      if size_only:
        pargs.append('--size-only')

      if preserve_mtime:
        pargs.append('-t')

      if progress:
        pargs.append('--progress')

      if delete_during:
        pargs.append('--delete-during')

      if prune_empty_dirs:
        pargs.append('--prune-empty-dirs')
    #    pargs.append('-m')

      if exclude_from:
        pargs.append("--exclude-from={}".format(exclude_from))

      if excluded_items:
        for excluded in excluded_items:
          pargs.append('--exclude={}'.format(excluded))

      if included_items:
        for included in included_items:
          pargs.append('--include={}'.format(included))

        if not excluded_items:
          pargs.append('--exclude=*')

      if filters:
        for item in filters:
          pargs.append('--filter={}'.format(item))

      if stats:
        pargs.append('--stats')

      if compute_size and not changes:
        changes = True

      if changes:
        pargs.append('-i')

      if args:
        pargs += args

      if source_user:
        source = '{}@{}'.format(source_user, source)

      if user:
        dest = '{}@{}'.format(user, dest)

      pargs += [source, dest]
      print(pargs)

      proc = subprocess.Popen(pargs, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

      encoding = 'utf-8'

      encoding = toolshed.platform.get_console_encoding()

      stdout = None

      if display:
        stdout = b""

        while True:
          output = proc.stdout.read(10)

          if not output:
            break

          stdout += output

          print(output.decode(encoding), end='', flush=True)

    #  proc = subprocess.run(pargs, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

      else:
        stdout = proc.stdout.read()

      if proc.stderr:
        print(proc.stderr.decode(encoding))

      if stdout:
    #     if display:
    #       print(stdout)

        res = {'stdout': stdout}

        if stats:
          found_items = self._nb_regex.findall(stdout)

          if found_items:
    #        print(found_items)

            for item in found_items:
              res['n' + item[0].decode()] = int(item[1])

            res['nchanges'] = res['nregular'] + res['ndeleted']

          search_res = self._size_regex.search(stdout)

          if search_res:
            res['size'] = int(search_res.group(1).replace(b',',b''))

        if changes:
          found_items = self._change_regex.findall(stdout)

          if found_items:
            items = []

            computed_size = 0

            if compute_size:
              if isinstance(source, pathlib.Path):
                source = source.parent
              else:
                source_str = source

                source = pathlib.Path(source)

                if '/' == source_str[-1]:
                  source = source.parent

            for item in found_items:
              item_path = item[1].decode()
              if compute_size:
                if self._existing_file_regex.match(item[0]):
                  computed_size += (source / item_path).stat().st_size

              items.append(item_path)

            res['changes'] = items

            if compute_size:
              res['size'] = computed_size

        if RSync._no_space_left_error_regex.findall(stdout):
          raise RSyncNotLeftSpaceError()
        elif RSync._error_regex.findall(stdout):
          raise RSyncError('sync failed...')

        return res

    # -----

    def size(self, source, exclude_from=None, filters=None):
      res = self.sync(source, '/tmp', dry_run=True, recursive=True, verbosity=2, size_only=True, exclude_from=exclude_from, filters=filters, stats=True, display=False)

      if res:
        return res['size']

    def size2(self, source, exclude_from=None, filters=None):
      res = self.sync(source, '/tmp', dry_run=True, recursive=True, verbosity=2, exclude_from=exclude_from, filters=filters, display=False, compute_size=True)

      if res:
        return res['size']

# -----

RSync._nb_regex = re.compile(b'^Number of ([a-z]+) files.*: ([0-9]+)', flags=re.MULTILINE)
RSync._size_regex = re.compile(b'^total size is ([0-9,]+)', flags=re.MULTILINE)
RSync._change_regex = re.compile(b'^(\*deleting|[\.c<>][df][\.cstTpoguax+]{9}) (.*)$', flags=re.MULTILINE)
RSync._existing_file_regex = re.compile(b'^([\.c<>][f])')
RSync._no_space_left_error_regex = re.compile(b'^rsync:.+ No space left on device', flags=re.MULTILINE)
RSync._error_regex = re.compile(b'^rsync error:', flags=re.MULTILINE)
