#!/usr/bin/python3
# coding: utf-8

import subprocess

class Docker:
  def __init__(self, encoding='utf-8'):
    self.encoding = encoding

    self.print_commands = True
    self.print_output = True

  def toggle_print_commands(self, state):
    self.print_commands = state

  def toggle_print_output(self, state):
    self.print_output = state

  def _run(self, args):
    pargs = ['docker', *args]

    if self.print_commands:
      print(pargs)

    result = subprocess.run(pargs, stdout=subprocess.PIPE, stderr=subprocess.STDOUT).stdout.decode(self.encoding)

    if self.print_output:
      print(result)

    return result

  def cp(self, source, dest, options=None):
    pargs = ['cp']

    if options:
      pargs += options

    pargs += [source, dest]

    self._run(pargs)

  def inspect(self, container_id, args=None, filters=None):
    pargs = ['inspect', container_id]

    if args:
      pargs += args

    if filters:
      for f in filters:
        pargs += ['-f', f]

    return self._run(pargs)

  def get_container_name(self, container_id):
    return self.inspect(container_id, filters=['{{.Name}}']).strip()[1:]
